import io from 'socket.io-client'
const DEVELOPMENT = 'http://localhost:8888'

let SERVER

SERVER = DEVELOPMENT

const sockets = io(SERVER, { autoConnect: true, forceNew: true })
export default sockets